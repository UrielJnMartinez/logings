
    // $('.formulario').animate({
    //     height: "toggle",
    //     'padding-top': 'toggle',
    //     'padding-bottom': 'toggle',
    //     opacity: 'toggle'
    // }, "slow");




    /* ======================================================================
  Author Custom JavaScript
====================================================================== */
// Loop through Array of Objects
var objPeople = [
	{ // Object @ 0 index
		username: "morgan",
		password: "gs"
	},
	{ // Object @ 1 index
		username: "gerardo",
		password: "gs"
	},
	{ // Object @ 2 index
		username: "rosario",
		password: "gs"
	}

]

var loginModal = new bootstrap.Modal(document.getElementById('loginGS'), {
	keyboard: false
  })



currentuser =localStorage.getItem('user');
pass=localStorage.getItem('password');
attempts = 0
	
function logIn() {
	var username = document.getElementById('user').value
	var password = document.getElementById('password').value
	attempts ++
	if (validateUser(username,password) == true){

		loginModal.hide()

		document.getElementById("custom-table").style.visibility="visible";
		document.getElementById("btn-logout").style.visibility="visible";
		document.getElementById("btn-login").style.visibility="hidden";
		localStorage.setItem('user', username);
		localStorage.setItem('password', password);
		localStorage.setItem('loginDate',new Date());
		// document.getElementById("username").innerHTML = username;
		attempts = 0
		return
	} 
	if (attempts == 3){
		swal(
			"Usuario o password incorrecto!",
			"Haz llegado al maximo numero de intentos, contacta a soporte tecnico",
			"error"

		)
	}else {

		swal("Lo siento!", "Usuario o password incorrecto!", "error");
	}
}
// validate user & password
function validateUser(user,password){
	for(var i = 0; i < objPeople.length; i++) {
		// check is user input matches username and password of a current index of the objPeople array
		if(user == objPeople[i].username && password == objPeople[i].password) {
			return true
		}
	}
}
// user authenticated
function isAuthenticated(user,pass) {
	checkTimeActive()
	if (validateUser(user,pass) == true){
		document.getElementById("custom-table").style.visibility="visible";
		document.getElementById("btn-logout").style.visibility="visible";
		// document.getElementById("btn-logout").style.display="block";

		document.getElementById("btn-login").style.visibility="hidden";
		
		return
	} else {
		loginModal.show()
	}
}
function addHoursToDate(date, hours) {
	return new Date(new Date(date).setHours(date.getHours() + hours));
  }
// Revisa si ya ha pasado mas de 3hrs cierra la seision del usuario
function checkTimeActive() {
	loginDate = localStorage.getItem('loginDate');
	expiredate = new Date(loginDate )
	currentTime = new Date()
	if ( loginDate != null && currentTime > addHoursToDate(expiredate,3)){
		logOut()
	}
	
}

// close session for user
function closeSession() {
	swal({
		title: "Quiere cerrar sesion?",
		icon: "warning",
		buttons: {
			cancel: "Cancelar",
			yes : {
				text : 'Si'
			}
			
		  },
		dangerMode: true,
	  })
	  .then((willDelete) => {
		if (willDelete) {
		  swal("sesion cerrada correctamente", {
			icon: "success",
		  });
		  logOut()
		} 
	  });
}

// log out session 
function logOut() {
	document.getElementById("custom-table").style.visibility="visible";
	attempts = 0
	localStorage.clear()
	location.reload();
}

isAuthenticated(currentuser,pass)